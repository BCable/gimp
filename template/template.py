#!/usr/bin/env python2.7

import gimp
from gimpfu import *

WIDTH=500
HEIGHT=500

#print(gimp.pdb.query("file_png_load"))
#print(gimp.pdb.query("file_png_save"))

# create image
img = gimp.Image(WIDTH, HEIGHT, RGB)
layer_000 = gimp.Layer(
    img, "layer_000", WIDTH, HEIGHT, RGBA_IMAGE, 100, NORMAL_MODE
)
img.add_layer(layer_000, 0)

# setup imagery
gimp.pdb.gimp_palette_set_foreground("#401040")
gimp.pdb.gimp_edit_fill(layer_000, FOREGROUND_FILL)

# update layer
layer_000.flush()
layer_000.update(0, 0, WIDTH, HEIGHT)

# save file
gimp.pdb.file_png_save(
    img, layer_000, "template.png", "raw_filename", 0, 9, 0, 0, 0, 0, 0
)

# memory cleanup
gimp.delete(img)
