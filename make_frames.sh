#!/bin/bash

script_file="scripts/${1}.py"

gimp -i --batch-interpreter=python-fu-eval -b 'execfile("'$script_file'"); pdb.gimp_quit(1)' > gimp.log
