all:
	echo 'Not available'

frames:
	mkdir -p frames

out:
	mkdir -p out

scripts:
	mkdir -p scripts

scripts/appel.py: frames scripts out
	cp simple_objects/main.py scripts/appel.py
	sed "s@\[NAME\]@appel@g" -i scripts/appel.py
	sed "s@\[INPUT_PNG\]@input_appel.png@g" -i scripts/appel.py
	sed "s@COLORS=\[\]@COLORS=\['#8E66E0','#8E66E0','#210E49','#210E49','#8E66E0','#504A8C','#8E66E0','#8CD5FF','#8E66E0','#5700A1','#8E66E0','#210E49','#210E49','#8E66E0','#8E66E0','#210E49','#210E49','#8E66E0','#504A8C','#8E66E0','#8CD5FF','#8E66E0','#5700A1','#8E66E0','#210E49','#210E49','#8E66E0','#FFFFFF'\]@g" -i scripts/appel.py
	sed "s@WIDTH=500@WIDTH=480@g" -i scripts/appel.py
	sed "s@COLOR_TRANSITIONS=0@COLOR_TRANSITIONS=0@g" -i scripts/appel.py

scripts/circle_random.py: frames scripts out
	cp simple_objects/main.py scripts/circle_random.py
	sed "s@\[NAME\]@circle@g" -i scripts/circle_random.py
	sed "s@\[INPUT_PNG\]@input_circle.png@g" -i scripts/circle_random.py

scripts/circle4_random.py: frames scripts out
	cp simple_objects/main.py scripts/circle4_random.py
	sed "s@\[NAME\]@circle4@g" -i scripts/circle4_random.py
	sed "s@\[INPUT_PNG\]@input_circle4.png@g" -i scripts/circle4_random.py

scripts/futurama_stupid.py: frames scripts out
	cp simple_objects/main.py futurama_stupid.py
	sed "s@\[NAME\]@futurama_stupid@g" -i scripts/futurama_stupid.py
	sed "s@\[INPUT_PNG\]@input_futurama_stupid.png@g" -i scripts/futurama_stupid.py
	sed "s@COLORS=\[\]@COLORS=\['#2020FF','#404080','#202040','#D0D0E0','#A0A0E0','#202040','#404080','#40E040','#202040','#D0D0E0','#40E040','#202040'\]@g" -i scripts/futurama_stupid.py
	sed "s@COLOR_TRANSITIONS=0@COLOR_TRANSITIONS=1@g" -i scripts/futurama_stupid.py

scripts/heart.py: frames scripts out
	cp simple_objects/main.py scripts/heart.py
	sed "s@\[NAME\]@heart@g" -i scripts/heart.py
	sed "s@\[INPUT_PNG\]@input_heart.png@g" -i scripts/heart.py
	sed "s@COLORS=\[\]@COLORS=\['#FBC9E2','#A96DDC','#DF2F5D','#E4FF00','#F370A2','#5700A1','#FF3E87','#FF0000','#FFFFFF','#EEFF63'\]@g" -i scripts/heart.py
	sed "s@COLOR_TRANSITIONS=0@COLOR_TRANSITIONS=2@g" -i scripts/heart.py

scripts/heart2.py: frames scripts out
	cp simple_objects/main.py scripts/heart2.py
	sed "s@\[NAME\]@heart2@g" -i scripts/heart2.py
	sed "s@\[INPUT_PNG\]@input_heart2.png@g" -i scripts/heart2.py
	sed "s@COLORS=\[\]@COLORS=\['#FBC9E2','#DF2F5D','#F370A2','#FF3E87','#FFFFFF'\]@g" -i scripts/heart2.py
	sed "s@COLOR_TRANSITIONS=0@COLOR_TRANSITIONS=4@g" -i scripts/heart2.py

scripts/heart_random.py: frames scripts out
	cp simple_objects/main.py scripts/heart_random.py
	sed "s@\[NAME\]@heart_random@g" -i scripts/heart_random.py
	sed "s@\[INPUT_PNG\]@input_heart.png@g" -i scripts/heart_random.py

scripts/heart2_random.py: frames scripts out
	cp simple_objects/main.py scripts/heart2_random.py
	sed "s@\[NAME\]@heart2_random@g" -i scripts/heart2_random.py
	sed "s@\[INPUT_PNG\]@input_heart2.png@g" -i scripts/heart2_random.py

scripts/square_rot.py: frames scripts out
	cp simple_objects/main.py scripts/square_rot.py
	sed "s@\[NAME\]@square_rot@g" -i scripts/square_rot.py
	sed "s@\[INPUT_PNG\]@input_square.png@g" -i scripts/square_rot.py
	sed "s@ROTATION=False@ROTATION=True@g" -i scripts/square_rot.py
	sed "s@ROTATION_TYPE=\[\]@ROTATION_TYPE=\[\"object\",\"layer\"\]@g" -i scripts/square_rot.py

frames/appel: scripts/appel.py
	mkdir -p frames/appel
	bash make_frames.sh appel

appel.gif: frames/appel
	bash make_video.sh appel gif

appel.mp4: frames/appel
	bash make_video.sh appel mp4

appel: appel.gif appel.mp4

frames/circle_random: scripts/circle_random.py
	mkdir -p frames/circle_random
	bash make_frames.sh circle_random

circle_random.gif: frames/circle_random
	bash make_video.sh circle_random gif

circle_random.mp4: frames/circle_random
	bash make_video.sh circle_random mp4

circle_random: circle_random.gif circle_random.mp4

frames/circle4_random: scripts/circle4_random.py
	mkdir -p frames/circle4_random
	bash make_frames.sh circle4_random

circle4_random.gif: frames/circle4_random
	bash make_video.sh circle4_random gif

circle4_random.mp4: frames/circle4_random
	bash make_video.sh circle4_random mp4

circle4_random: circle4_random.gif circle4_random.mp4

frames/futurama_stupid: scripts/futurama_stupid.py
	mkdir -p frames/futurama_stupid
	bash make_frames.sh futurama_stupid

futurama_stupid.gif: frames/futurama_stupid
	bash make_video.sh futurama_stupid gif

futurama_stupid.mp4: frames/futurama_stupid
	bash make_video.sh futurama_stupid mp4

futurama_stupid: futurama_stupid.gif futurama_stupid.mp4

frames/heart: scripts/heart.py
	mkdir -p frames/heart
	bash make_frames.sh heart

heart.gif: frames/heart
	bash make_video.sh heart gif

heart.mp4: frames/heart
	bash make_video.sh heart mp4

heart: heart.gif heart.mp4

frames/heart2: scripts/heart2.py
	mkdir -p frames/heart2
	bash make_frames.sh heart2

heart2.gif: frames/heart2
	bash make_video.sh heart2 gif

heart2.mp4: frames/heart2
	bash make_video.sh heart2 mp4

frames/heart_random: scripts/heart_random.py
	mkdir -p frames/heart_random
	bash make_frames.sh heart_random

heart_random.gif: frames/heart_random
	bash make_video.sh heart_random gif

heart_random.mp4: frames/heart_random
	bash make_video.sh heart_random mp4

heart_random: heart_random.gif heart_random.mp4

frames/heart2_random: scripts/heart2_random.py
	mkdir -p frames/heart2_random
	bash make_frames.sh heart2_random

heart2_random.gif: frames/heart2_random
	bash make_video.sh heart2_random gif

heart2_random.mp4: frames/heart2_random
	bash make_video.sh heart2_random mp4

heart2_random: heart2_random.gif heart2_random.mp4

frames/square_rot: scripts/square_rot.py
	mkdir -p frames/square_rot
	bash make_frames.sh square_rot

square_rot.gif: frames/square_rot
	bash make_video.sh square_rot gif

square_rot.mp4: frames/square_rot
	bash make_video.sh square_rot mp4

square_rot: square_rot.gif square_rot.mp4

clean:
	rm -f *.pyc __pycache__
	rm -f gimp.log
	rm -rf frames out scripts
