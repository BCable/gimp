#!/bin/bash

name="$1"
extension="$2"

ffmpeg -i frames/$name/frame_%03d.png -r 50 -pix_fmt yuv420p out/$name.$extension
