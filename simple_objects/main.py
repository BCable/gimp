#!/usr/bin/env python2.7

# Simple object animation framework in GIMP
# Author: Brad Cable
# License: MIT

import gimp
from gimpfu import *

import random
from copy import deepcopy

WIDTH=500
HEIGHT=500
THICKNESS=4
ROTATION=False
ROTATION_ANGLE=0
ROTATION_TYPE=[]
COLORS=[]
COLOR_TRANSITIONS=0
LAYERS=100
FRAMES=None

# load source image
path = "input/[INPUT_PNG]"
src = pdb.file_png_load(path, path)
layer_src = src.active_layer

# random color generation
def gen_randcolor():
	return "#{0:0>2}{1:0>2}{2:0>2}".format(
		hex(random.randint(40,255))[2:],
		hex(random.randint(40,255))[2:],
		hex(random.randint(40,255))[2:]
	)

# generate transitions
def get_transition(shade_a, shade_b):
	fade = (shade_b-shade_a)/(COLOR_TRANSITIONS+1)

	ret = [shade_a]
	for i in range(0, COLOR_TRANSITIONS+1):
		ret.append(shade_a + (fade * (i+1)))

	return ret

# transition between two colors
def gen_transition(color_a, color_b):
	cola_r = int(color_a[1:3], 16)
	cola_g = int(color_a[3:5], 16)
	cola_b = int(color_a[5:7], 16)

	colb_r = int(color_b[1:3], 16)
	colb_g = int(color_b[3:5], 16)
	colb_b = int(color_b[5:7], 16)

	col_rt = get_transition(cola_r, colb_r)
	col_gt = get_transition(cola_g, colb_g)
	col_bt = get_transition(cola_b, colb_b)

	ret = []
	for i in range(0, COLOR_TRANSITIONS+1):
		ret.append("#{0:0>2}{1:0>2}{2:0>2}".format(
			hex(int(col_rt[i]))[2:],
			hex(int(col_gt[i]))[2:],
			hex(int(col_bt[i]))[2:]
		))

	return ret

# flushing of layers
def flush(layer):
	layer.flush()
	layer.update(0, 0, WIDTH, HEIGHT)

# COLORS
color_palette = COLORS

# random list
if len(COLORS) == 0:
	color_palette = [gen_randcolor() for i in range(0, LAYERS)]

# color transition
elif COLOR_TRANSITIONS != 0:
	color_palette = deepcopy(COLORS)
	color_palette.append(color_palette[0])

	new_color_palette = []
	for i in range(0, len(COLORS)):
		new_color_palette.extend(gen_transition(
			color_palette[i], color_palette[i+1]
		))

	color_palette = new_color_palette

# fill in FRAMES as exact number of colors
if FRAMES is None:
	FRAMES = len(color_palette)

# create frame images
for i in range(0, FRAMES):
	print("Processing Frame: {}/{}".format(i+1, FRAMES))

	frame = gimp.Image(WIDTH, HEIGHT, RGB)

	layer_bg = gimp.Layer(frame, "layer_bg",
		WIDTH, HEIGHT, RGBA_IMAGE, 100, NORMAL_MODE
	)
	#frame.add_layer(layer_bg, 0)
	pdb.gimp_image_insert_layer(frame, layer_bg, None, 0)

	# set background
	pdb.gimp_palette_set_background("#FFFFFF")
	pdb.gimp_palette_set_foreground("#FFFFFF")
	pdb.gimp_edit_fill(layer_bg, FOREGROUND_FILL)

	# flush layer
	flush(layer_bg)

	# create center layer
	layer_center = gimp.Layer(frame, "layer_center",
		WIDTH, HEIGHT, RGBA_IMAGE, 100, NORMAL_MODE
	)
	#frame.add_layer(layer_center, 0)
	pdb.gimp_image_insert_layer(frame, layer_center, None, 0)

	# setup imagery
	pdb.gimp_selection_all(frame)
	pdb.gimp_edit_copy(layer_src)
	pdb.gimp_edit_paste(layer_center, True)

	# flush layer
	flush(layer_center)

	# rotation
	if ROTATION is True and "object" in ROTATION_TYPE:
		rotation = ROTATION_ANGLE

		# generate automatically for perfect looping
		if ROTATION_ANGLE == 0:
			rotation = i*(360.0/FRAMES)

		# debug print
		print("Rotation: {}, {}, {}, {}".format(
			ROTATION, ROTATION_TYPE, ROTATION_ANGLE, rotation
		))

		pdb.gimp_context_set_interpolation(layer_center)
		pdb.gimp_context_set_transform_direction(layer_center)
		pdb.gimp_context_set_transform_resize(layer_center)
		layer_center = pdb.gimp_item_transform_rotate(
			layer_center, rotation, True, 0, 0
		)

		# flush layer
		flush(layer_center)

	# reset
	pdb.gimp_selection_none(frame)

	# loop layers
	DEFINED_LAYERS = 2
	layer_tocopy = layer_center
	for j in range(DEFINED_LAYERS, LAYERS+DEFINED_LAYERS):
		layer_group = gimp.GroupLayer(frame)
		frame.add_layer(layer_group, 0)
		flush(layer_group)

		layer_new = gimp.Layer(frame, "layer_{0:0>3}".format(j),
			WIDTH, HEIGHT, RGBA_IMAGE, 100, NORMAL_MODE
		)
		pdb.gimp_image_insert_layer(frame, layer_new, layer_group, 0)
		flush(layer_new)

		# expand selection accordingly
		pdb.gimp_image_raise_layer_to_top(frame, layer_center)
		pdb.gimp_selection_none(frame)

		# select transparency
		#pdb.gimp_by_color_select(
			#layer_center, (0,0,0), 10, 0, False, False, 0, True
		#)
		pdb.gimp_selection_layer_alpha(layer_center)
		#pdb.gimp_selection_invert(frame)

		# debug info
		print("Selection Bounds ({}): {}".format(
			j, pdb.gimp_selection_bounds(frame)
		))
		print("Thickness: {}".format(THICKNESS*(j-DEFINED_LAYERS+1)))

		# grow selection
		pdb.gimp_selection_grow(frame, THICKNESS*(j-DEFINED_LAYERS+1))
		print("Selection Bounds (Grow): {}".format(
			pdb.gimp_selection_bounds(frame)
		))

		# set color and fill
		fill_color = color_palette[(i-j) % len(color_palette)]
		print("Fill Color: {}".format(fill_color))
		pdb.gimp_palette_set_foreground(fill_color)
		pdb.gimp_bucket_fill(
			layer_new, FG_BUCKET_FILL, NORMAL_MODE, 100, 10,
			False, 0, 0
		)

		# reset settings
		pdb.gimp_selection_none(frame)
		pdb.gimp_palette_set_foreground("#40FF40")
		pdb.gimp_palette_set_background("#804080")

		# create working layer
		layer_work = gimp.Layer(frame, "layer_work",
			WIDTH, HEIGHT, RGBA_IMAGE, 100, NORMAL_MODE
		)
		pdb.gimp_image_insert_layer(frame, layer_work, layer_group, 1)

		# copy layer onto working layer
		pdb.gimp_selection_all(frame)
		pdb.gimp_edit_copy(layer_tocopy)
		pdb.gimp_edit_paste(layer_work, True)

		# remove old copy layer if necessary
		if layer_tocopy != layer_center:
			pdb.gimp_image_remove_layer(frame, layer_tocopy)

		# raise layers for organization
		pdb.gimp_image_raise_layer_to_top(frame, layer_new)
		pdb.gimp_image_raise_layer_to_top(frame, layer_work)

		# merge layer group
		layer_tocopy = pdb.gimp_image_merge_layer_group(frame, layer_group)
		layer_group = None

	# flatten image
	pdb.gimp_image_raise_layer_to_top(frame, layer_tocopy)
	pdb.gimp_image_raise_layer_to_top(frame, layer_center)
	layer_rs = pdb.gimp_image_flatten(frame)

	# flush layers
	flush(layer_rs)

	# save files
	pdb.file_png_save(
		frame, layer_rs,
		"frames/[NAME]/frame_{0:0>3}.png".format(i),
		"raw_filename",
		0, 9, 0, 0, 0, 0, 0
	)

	# memory cleanup
	gimp.delete(frame)

# memory cleanup
gimp.delete(src)
